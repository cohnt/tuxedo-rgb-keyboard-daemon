#!/bin/bash
set -e
set -x

SCRIPT_DIR=$(pwd)

INSTALL_DIR=$(pwd)

./preflight-check.sh

echo "Creating new virtual environment"
cd "$INSTALL_DIR"
python3 -m venv tkd_env
source tkd_env/bin/activate
pip3 install --upgrade pip
pip3 install wheel

echo "Installing python dependencies"
cd "$INSTALL_DIR"
source tkd_env/bin/activate
pip3 install -r requirements_pip.txt

echo "Creating daemon executable"
tee tkd << END
#!/bin/bash
SCRIPT_DIR="$(pwd)"
source "$SCRIPT_DIR"/tkd_env/bin/activate

python3 "$SCRIPT_DIR"/daemon.py
END
chmod +x tkd
sudo mv tkd /usr/bin

echo "Creating client"
tee tkc << END
#!/bin/bash
mkdir -p ~/.config/tuxedo-keyboard-daemon/

echo -n "\$@" >> ~/.config/tuxedo-keyboard-daemon/tuxedo-keyboard-rgb-control
END
chmod +x tkc
sudo mv tkc /usr/bin

echo "Creating config dir with necessary files"
mkdir -p ~/.config/tuxedo-keyboard-daemon/
cp -r custom ~/.config/tuxedo-keyboard-daemon/

echo "Creating necessary udev rules"
sudo tee /etc/udev/rules.d/99-tkd.rules << END
# RGB Keyboard
SUBSYSTEMS=="usb", ATTRS{idVendor}=="048d", ATTRS{idProduct}=="6004", MODE:="0666"
SUBSYSTEMS=="usb", ATTRS{idVendor}=="048d", ATTRS{idProduct}=="ce00", MODE:="0666"

# Tuxedo Keyboard
SUBSYSTEM=="leds", ACTION=="add", RUN+="/bin/chgrp -R leds /sys%p", RUN+="/bin/chmod -R g=u /sys%p"
SUBSYSTEM=="leds", ACTION=="change", ENV{TRIGGER}!="none", RUN+="/bin/chgrp -R leds /sys%p", RUN+="/bin/chmod -R g=u /sys%p"
END

echo "Creating necessary groups"
sudo groupadd leds
sudo usermod -a -G leds $USER

echo "Reloading udev"
sudo udevadm control --reload
sudo udevadm trigger
