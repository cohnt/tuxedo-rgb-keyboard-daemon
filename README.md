# Tuxedo RGB keyboard daemon

99% of the code was copied from https://github.com/salihmarangoz/system_tray_extensions

This version strips away gtk integration and exposes a simple daemon and client to interact with the keyboard rgb lights

## Installation

If you are on a arch based system, run `./install_arch.sh`. If you are on a debian based one, run `./install_debian.sh`.

To run this automatically, tkd needs to be executed with a graphical context. Executing it on startup (https://askubuntu.com/a/544431) or `exec`ing it in your i3 config. Whatever floats your goat. If you manage to run this with systemd, let me know.

## Usage

To run the daemon, execute `tkd`. Once the daemon is running, use the `tkc` command to interact with the daemon.

### Available client commands:
`tkc mode custom your-script-here`, where `your-script-here` is a script in `~/.config/tuxedo-keyboard-daemon/custom`. Examples:
* `tkc mode custom keyboard_mouse`
* `tkc mode custom reflect_screen`

`tkc mode colour r,g,b`, here `r`, `g` and `b` are floats between 0 and 1. Examples:
* `tkc mode colour 1,1,1` for white
* `tkc mode colour 0.4,0,0.8` for purplish colour