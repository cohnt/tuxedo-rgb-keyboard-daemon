python3 --version

if [[ ! $? -eq 0 ]]; then
	echo "python3 is not installed. Please install it first-"
	exit 1
fi

pip3 --version

if [[ ! $? -eq 0 ]]; then
	echo "pip3 is not installed. Please install it first."
	exit 1
fi

if [[ ! -d /etc/udev/rules.d ]]; then
	echo "Directory '/etc/udev/rules.d/' not found."
	echo "Exiting"
	exit 1
fi

if [[ -f /etc/udev/rules.d/99-tkd.rules ]]; then
	echo "File '/etc/udev/rules.d/99-ste.rules' already exists, please rename it."
	echo "Exiting"
	exit 1
fi
